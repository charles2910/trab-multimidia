.PHONY: all clean

all:
	$(MAKE) -C src
	cp src/imgcmp $(CURDIR)

clean:
	rm -f imgcmp
	$(MAKE) clean -C src
