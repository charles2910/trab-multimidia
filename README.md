# Trabalho 1 - SCC0661 - Multimídia e Hipermídia
## Alunos
- Carlos Henrique Lima Melara - 9805380
- Marina Nastri da Costa Pereira Rodero - 10093702
- Nicolas Ribeiro Batistuti - 10408351

## Descrição

Este trabalho tem como objetivo desenvolver um compressor e o descompressor de
imagens utilizando os conceitos vistos em sala de aula (codificação por
diferença, método estático e etc) além de também calcular a taxa de compressão.

## Construção

O projeto conta com um arquivo Makefile contendo as instruções de compilação.

### Compilando
As instruções de compilação foram verificadas no seguinte ambiente:

- Debian testing;
- gcc (Debian 12.2.0-10) 12.2.0;
- GNU Make 4.3;

Para compilar, basta executar:

```bash
make
```

### Limapando o diretório
Para remover tanto o binário quantos os arquivos objeto, basta fazer:

```bash
make clean
```

## Exemplos
O código possui salvo em conjunto uma seleção de imagens préviamente conhecidas
estruturalmente para facilitar a avaliação de desempenho do software. Elas se
encontram no diretório `samples/`.

## Uso

Invocando o `makefile` com `make`, é gerado um binário `imgcmp` o qual é a
nossa aplicação de fato.

### Compressão

Para comprimir imagens, basta usar a opção `-c` seguida do caminho da imagem
bmp e do nome do arquivo comprimido. Um exemplo é mostrado abaixo.

```bash
./imgcmp -c samples/colors.bmp colors.bin
```

### Descompressão

Para descomprimir imagens, basta usar a opção `-d` seguida do caminho do
arquivo comprimido seguido do nome da imagem bmp a ser gerada. Um exemplo é
mostrado abaixo.

```bash
./imgcmp -d colors.bin colors.bmp
```

### Outras Opções

O programa também conta com a opção `-h` para imprimir um breve texto de ajuda.

```bash
./imgcmp -h
```
