#ifndef _SUPPORT_H_
#define _SUPPORT_H_

#include "bitmap.h"

//declaracao das funcoes
int alloc_bmp_image(BMPIMAGE *bmpimg, int size);
int free_bmp_image(BMPIMAGE *bmpimg);
int alloc_ycbcr_image(YCbCr *ycbcrimg, int size);
int free_ycbcr_image(YCbCr *ycbcrimg);
int alloc_ycbcr_vector_image(YCbCr_VECTOR *ycbcrimg, int width, int height, MODE sample_mode);
int free_ycbcr_sampled_image(YCbCr_VECTOR *ycbcrimg);

#endif