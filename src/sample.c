#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "bitmap.h"
#include "r2y.h"
#include "sample.h"

//funcao que converte do padrao 444 para o padrao 422
void four44_2_four22(YCbCr *image_YCbCr, YCbCr_VECTOR *image_422, BITMAPINFOHEADER *info_header)
{
    int tam = info_header->Height * info_header->Width;
    image_422->height = info_header->Height;
    image_422->width = info_header->Width;
    image_422->Y = (int *) malloc(tam * sizeof(int));
    image_422->Cb = (int *) malloc(tam / 2 * sizeof(int));
    image_422->Cr = (int *) malloc(tam / 2 * sizeof(int));

    for (int i = 0; i < tam; i++) {
        image_422->Y[i] = image_YCbCr[i].Y;
        if (i % 2 == 0) {
            image_422->Cb[i/2] = image_YCbCr[i].Cb;
            image_422->Cr[i/2] = image_YCbCr[i].Cr;
        }
    }

    return;
}
//funcao que converte do padrao 422 para o padrao 444
void four22_2_four44(YCbCr **image_YCbCr, YCbCr_VECTOR *image_422, BITMAPINFOHEADER *info_header)
{
    int tam = info_header->Height * info_header->Width;

    *image_YCbCr = (YCbCr *) malloc(tam * sizeof(YCbCr));

    for (int i = 0; i < tam; i++) {
        (*image_YCbCr)[i].Y = image_422->Y[i];
        (*image_YCbCr)[i].Cb = image_422->Cb[i / 2];
        (*image_YCbCr)[i].Cr = image_422->Cr[i / 2];
    }

    return;
}
