#ifndef _BITMAP_H_
#define _BITMAP_H_

#include <stdio.h>

typedef enum {S422, S444, S422_DIFF, S444_DIFF} MODE;

//struct de valores Red, Green e Blue do pixel
typedef struct
{
    unsigned char R;
    unsigned char G;
    unsigned char B;
} BMPIMAGE;

//struct de valores Y.Cb e Cr do pixel
typedef struct
{
    unsigned char Y;
    unsigned char Cb;
    unsigned char Cr;
} YCbCr;

//struct de informacoes do vetor da imagem no formato Y Cb e Cr
typedef struct
{
    int *Y;
    int *Cb;
    int *Cr;
    unsigned int width;
    unsigned int height;
    MODE mode;
} YCbCr_VECTOR;

//struct fornecida previamente contendo parametros padronizados do cabecalho arquivo BMP
typedef struct                       /**** BMP file header structure ****/
{
    unsigned short Type;           /* Magic number for file */
    unsigned int   Size;           /* Size of file */
    unsigned short Reserved1;      /* Reserved */
    unsigned short Reserved2;      /* ... */
    unsigned int   OffBits;        /* Offset to bitmap data */
} BITMAPFILEHEADER;

#  define BF_TYPE 0x4D42             /* "MB" */

//struct fornecida previamente contendo parametros padronizados do formato de arquivo BMP
typedef struct                       /**** BMP file info structure ****/
{
    unsigned int   Size;           /* Size of info header */
    int            Width;          /* Width of image */
    int            Height;         /* Height of image */
    unsigned short Planes;         /* Number of color planes */
    unsigned short BitCount;       /* Number of bits per pixel */
    unsigned int   Compression;    /* Type of compression to use */
    unsigned int   SizeImage;      /* Size of image data */
    int            XResolution;    /* X pixels per meter */
    int            YResolution;    /* Y pixels per meter */
    unsigned int   NColours;        /* Number of colors used */
    unsigned int   ImportantColours;   /* Number of important colors */
} BITMAPINFOHEADER;

//struct contendo os parametro de arquivo e informacao do cabecalho do arquivo da imagem
typedef struct {
    BITMAPFILEHEADER bmp_file_header;
    BITMAPINFOHEADER bmp_info_header;
    char *filename;
} BITMAPHEADER;

//declaracao das funcoes
void loadBMPHeaders(FILE *fp, BITMAPHEADER *bmp_header);
void storeBMPHeaders(FILE *output, BITMAPHEADER *bmp_header);

void printHeaders (BITMAPFILEHEADER *FileHeader,  BITMAPINFOHEADER *InfoHeader);

void readInfoHeader(FILE *F, BITMAPINFOHEADER *INFO_H);
void storeInfoHeader(FILE *F, BITMAPINFOHEADER *INFO_H);

void readHeader(FILE *F, BITMAPFILEHEADER *H);
void storeHeader(FILE *F, BITMAPFILEHEADER *H);

int loadBMPImage(FILE *input, BITMAPHEADER *bmp_header, BMPIMAGE **Image);
int storeBMPImage(FILE *output, BITMAPHEADER *bmp_header, BMPIMAGE *Image);


#endif
