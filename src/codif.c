#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "bitmap.h"
#include "codif.h"
#include "sample.h"

extern int mode;
//padroes para o metodo da codificao de huffman
const HUFFMAN_ENTRY huffman_table[HUFFMAN_TABLE_SIZE] = {
    {
        .limits = {0, 0},
        .category = 0,
        .total_size = 3,
        .manti_size = 0,
        .prefix = 2,
    },
    {
        .limits = {1, 1},
        .category = 1,
        .total_size = 4,
        .manti_size = 1,
        .prefix = 3,
    },
    {
        .limits = {2, 3},
        .category = 2,
        .total_size = 5,
        .manti_size = 2,
        .prefix = 4,
    },
    {
        .limits = {4, 7},
        .category = 3,
        .total_size = 5,
        .manti_size = 3,
        .prefix = 0,
    },
    {
        .limits = {8, 15},
        .category = 4,
        .total_size = 7,
        .manti_size = 4,
        .prefix = 5,
    },
    {
        .limits = {16, 31},
        .category = 5,
        .total_size = 8,
        .manti_size = 5,
        .prefix = 6,
    },
    {
        .limits = {32, 63},
        .category = 6,
        .total_size = 10,
        .manti_size = 6,
        .prefix = 14,
    },
    {
        .limits = {64, 127},
        .category = 7,
        .total_size = 12,
        .manti_size = 7,
        .prefix = 30,
    },
    {
        .limits = {128, 255},
        .category = 8,
        .total_size = 14,
        .manti_size = 8,
        .prefix = 62,
    },
};

//funcao que converte do padrao 422 para a codificacao por diferencas
void four22_2_diff(YCbCr_VECTOR *image_422, YCbCr_VECTOR *image_422_diff)
{
    int tam = image_422->height * image_422->width;

    image_422_diff->Y = (int *) malloc(tam * sizeof(int));
    image_422_diff->Cb = (int *) malloc((tam / 2) * sizeof(int));
    image_422_diff->Cr = (int *) malloc((tam / 2) * sizeof(int));

    image_422_diff->Y[0] = (int) image_422->Y[0];
    image_422_diff->Cb[0] = (int) image_422->Cb[0];
    image_422_diff->Cr[0] = (int) image_422->Cr[0];
    image_422_diff->width = image_422->width;
    image_422_diff->height = image_422->height;

    for(int i = 1; i < tam; i++) {
        image_422_diff->Y[i] = (int) (image_422->Y[i] - image_422->Y[i - 1]);
        if(i < (tam / 2)) {
            image_422_diff->Cb[i] = (int) (image_422->Cb[i] - image_422->Cb[i - 1]);
            image_422_diff->Cr[i] = (int) (image_422->Cr[i] - image_422->Cr[i - 1]);
            if(mode == 3)
                printf("[four22_2_diff] Info: for Cb and Cr tam == %d\n", i);
        }
    }
}

//funcao que passa da codificacao por diferenca para o padrao 422
void diff_2_four22(YCbCr_VECTOR *image_422, YCbCr_VECTOR *image_422_diff)
{
    int tam = image_422_diff->height * image_422_diff->width;

    if(mode == 3)
        printf("[diff_2_four22] Info: size is %d\n", tam);

    image_422->Y = (int *) malloc(tam * sizeof(int));
    image_422->Cb = (int *) malloc((tam / 2) * sizeof(int));
    image_422->Cr = (int *) malloc((tam / 2) * sizeof(int));

    image_422->Y[0] = (int) image_422_diff->Y[0];
    image_422->Cb[0] = (int) image_422_diff->Cb[0];
    image_422->Cr[0] = (int) image_422_diff->Cr[0];
    image_422->width =  image_422_diff->width;
    image_422->height = image_422_diff->height;

    for(int i = 1; i < tam; i++) {
        image_422->Y[i] = image_422_diff->Y[i] + image_422->Y[i - 1];
        if(i < (tam / 2)) {
            image_422->Cb[i] = image_422_diff->Cb[i] + image_422->Cb[i - 1];
            image_422->Cr[i] = image_422_diff->Cr[i] + image_422->Cr[i - 1];
        }
    }
}

//funcao que converte da codificacao por diferenca para a codificacao de huffman
void diff_2_huffman(YCbCr_VECTOR *image_422_diff, FOUR22_HUFFMAN *image_422_huffman)
{
    int tam = image_422_diff->height * image_422_diff->width;
    HUFFMAN_ENTRY *curr_entry;
    unsigned short curr_mantissa = 0;

    image_422_huffman->Y = (HUFFMAN_VALUE *) malloc(tam * sizeof(HUFFMAN_VALUE));
    image_422_huffman->Cb = (HUFFMAN_VALUE *) malloc(tam / 2 * sizeof(HUFFMAN_VALUE));
    image_422_huffman->Cr = (HUFFMAN_VALUE *) malloc(tam / 2 * sizeof(HUFFMAN_VALUE));
    image_422_huffman->width =  image_422_diff->width;
    image_422_huffman->height = image_422_diff->height;

    for(int i = 0; i < tam; i++) {
        curr_entry = (HUFFMAN_ENTRY *) get_huffman_entry_from_value(image_422_diff->Y[i]);
        curr_mantissa = get_huffman_value(curr_entry, image_422_diff->Y[i]);
        image_422_huffman->Y[i].mantissa = curr_mantissa;
        image_422_huffman->Y[i].prefix = curr_entry->prefix;
        image_422_huffman->Y[i].total_size = curr_entry->total_size;
        image_422_huffman->Y[i].manti_size = curr_entry->manti_size;

        if(i < (tam / 2)) {
            curr_entry = (HUFFMAN_ENTRY *) get_huffman_entry_from_value(image_422_diff->Cb[i]);
            curr_mantissa = get_huffman_value(curr_entry, image_422_diff->Cb[i]);
            image_422_huffman->Cb[i].mantissa = curr_mantissa;
            image_422_huffman->Cb[i].prefix = curr_entry->prefix;
            image_422_huffman->Cb[i].total_size = curr_entry->total_size;
            image_422_huffman->Cb[i].manti_size = curr_entry->manti_size;

            curr_entry = (HUFFMAN_ENTRY *) get_huffman_entry_from_value(image_422_diff->Cr[i]);
            curr_mantissa = get_huffman_value(curr_entry, image_422_diff->Cr[i]);
            image_422_huffman->Cr[i].mantissa = curr_mantissa;
            image_422_huffman->Cr[i].prefix = curr_entry->prefix;
            image_422_huffman->Cr[i].total_size = curr_entry->total_size;
            image_422_huffman->Cr[i].manti_size = curr_entry->manti_size;
        }
    }

    return;
}
//funcao que converte da codificacao de huffman para a codificacao por diferenca
void huffman_2_diff(YCbCr_VECTOR *image_422_diff, FOUR22_HUFFMAN *image_422_huffman)
{
    int tam = image_422_huffman->height * image_422_huffman->width;
    unsigned char prefix;
    HUFFMAN_ENTRY *curr_entry = NULL;

    image_422_diff->Y = (int *) malloc(tam * sizeof(int));
    image_422_diff->Cb = (int *) malloc(tam / 2 * sizeof(int));
    image_422_diff->Cr = (int *) malloc(tam / 2 * sizeof(int));
    image_422_diff->width = image_422_huffman->width;
    image_422_diff->height = image_422_huffman->height;

    for(int i = 0; i < tam; i++) {
        prefix = image_422_huffman->Y[i].prefix;
        curr_entry = (HUFFMAN_ENTRY *) get_huffman_entry_from_prefix(prefix);
        if(curr_entry == NULL) {
                printf("[huffman_2_diff] Error: invalid prefix (%u) for pixel %d (%d, %d) component Y\n", prefix, i, i / image_422_diff->width, i % image_422_diff->width);
                exit(-1);
            }
        image_422_diff->Y[i] = get_value_from_huffman(curr_entry, image_422_huffman->Y[i].mantissa);

        if(i < (tam / 2)) {
            prefix = image_422_huffman->Cb[i].prefix;
            curr_entry = (HUFFMAN_ENTRY *) get_huffman_entry_from_prefix(image_422_huffman->Cb[i].prefix);
            if(curr_entry == NULL) {
                printf("[huffman_2_diff] Error: invalid prefix (%u) for pixel (%d, %d) component Cb\n", prefix, i / image_422_diff->width, i % image_422_diff->width);
                exit(-1);
            }
            image_422_diff->Cb[i] = get_value_from_huffman(curr_entry, image_422_huffman->Cb[i].mantissa);

            prefix = image_422_huffman->Cr[i].prefix;
            curr_entry = (HUFFMAN_ENTRY *) get_huffman_entry_from_prefix(image_422_huffman->Cr[i].prefix);
            if(curr_entry == NULL) {
                printf("[huffman_2_diff] Error: invalid prefix (%u) for pixel (%d, %d) component Cr\n", prefix, i / image_422_diff->width, i % image_422_diff->width);
                exit(-1);
            }
            image_422_diff->Cr[i] = get_value_from_huffman(curr_entry, image_422_huffman->Cr[i].mantissa);
        }
    }

    return;
}

const HUFFMAN_ENTRY * get_huffman_entry_from_value(int value)
{
    int mod_value = abs(value);

    for(int i = 0; i < HUFFMAN_TABLE_SIZE; i++) {
        int upper = huffman_table[i].limits[1];
        int lower = huffman_table[i].limits[0];

        if(mod_value >= lower && mod_value <= upper)
            return &huffman_table[i];
    }

    if(mode == 3)
        printf("[get_huffman_entry_from_value] Error: couldn't find entry from value %d\n", value);

    return NULL;
}

const HUFFMAN_ENTRY * get_huffman_entry_from_prefix(short prefix)
{
    for(int i = 0; i < HUFFMAN_TABLE_SIZE; i++) {
        if(prefix == huffman_table[i].prefix)
            return &huffman_table[i];
    }

    if(mode == 3)
        printf("[get_huffman_entry_from_prefix] Error: couldn't find entry from prefix %d\n", prefix);

    return NULL;
}

//funcao que obtem os parametro para a codificacao de huffman
unsigned char get_huffman_value(HUFFMAN_ENTRY *huffman_entry, int value)
{
    unsigned short huffman_value = 0;

    if(huffman_entry == NULL) {
        printf("[get_huffman_value] Error: huffman_entry is NULL\n");
        exit(-1);
    }

    if(value < 0)
        huffman_value = (unsigned char) (value + huffman_entry->limits[1]);
    else
        huffman_value = (unsigned char) value;

    return huffman_value;
}

//obtem os parametros que foram utilizados em uma codificacao de huffman
int get_value_from_huffman(HUFFMAN_ENTRY *huffman_entry, unsigned char huffman_value) {
    int value = 0;

    if(huffman_entry == NULL) {
        printf("[get_huffman_value] Error: huffman_entry is NULL\n");
        exit(-1);
    }

    if(huffman_value < huffman_entry->limits[0])
        value = (int) huffman_value - huffman_entry->limits[1];
    else
        value = (int) huffman_value;

    return value;
}

//funcao que compara os coeficientes com a tabela de huffman
int check_prefix_match(unsigned char prefix)
{
    for(int i = 0; i < HUFFMAN_TABLE_SIZE; i++) {
        if(prefix == huffman_table[i].prefix)
            return 1;
    }

    return 0;
}

//funcao que obtem os prefixos da codificacao de huffman
int get_prefix_index(unsigned char prefix)
{
    for(int i = 0; i < HUFFMAN_TABLE_SIZE; i++) {
        if(prefix == huffman_table[i].prefix)
            return i;
    }

    if(mode == 3)
        printf("[get_prefix_index] Error: couldn't find prefix index for %c\n", prefix); // prefixo invalido

    return -1;
}

//obtem o tamanho total
unsigned char get_total_size_from_index(int index)
{
    if(index < HUFFMAN_TABLE_SIZE)
        return huffman_table[index].total_size;
    else
        return -1;
}

unsigned char get_mantissa_size_from_index(int index)
{
    if(index < HUFFMAN_TABLE_SIZE)
        return huffman_table[index].manti_size;
    else
        return -1;
}