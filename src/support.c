#include <stdio.h>
#include <stdlib.h>

#include "bitmap.h"
#include "codif.h"
#include "sample.h"
#include "support.h"

extern int mode;
//funcao que aloca a imagem em BMP na memoria do sistema
int alloc_bmp_image(BMPIMAGE *bmpimg, int size)
{
    if(size < 1) {
        if(mode == 3)
            printf("[alloc_bmp_image] Error: invalid size (%d)\n", size);
        return -1;
    }

    bmpimg = (BMPIMAGE *) malloc(size * sizeof(BMPIMAGE));

    if(!bmpimg) {
        if(mode == 3)
            printf("[alloc_bmp_image] Error: malloc error (%p)\n", bmpimg);
        return -1;
    }

    return 0;
}
//funcao que libera da memoria do sistema da imagem em BMP previamente alocada
int free_bmp_image(BMPIMAGE *bmpimg)
{
    if(!bmpimg) {
        if(mode == 3)
            printf("[free_bmp_image] Error: address is NULL (%p)\n", bmpimg);
        return -1;
    }

    free(bmpimg);
    return 0;
}

//funcao que aloca a imagem em YCbCr na memoria do sistema
int alloc_ycbcr_image(YCbCr *ycbcrimg, int size)
{
    if(size < 1) {
        if(mode == 3)
            printf("[alloc_ycbcr_image] Error: invalid size (%d)\n", size);
        return -1;
    }

    ycbcrimg = (YCbCr *) malloc(size * sizeof(YCbCr));

    if(!ycbcrimg) {
        if(mode == 3)
            printf("[alloc_ycbcr_image] Error: malloc error (%p)\n", ycbcrimg);
        return -1;
    }

    return 0;
}
//funcao que libera da memoria do sistema a imagem em YCbCr previamente alocada
int free_ycbcr_image(YCbCr *ycbcrimg)
{
    if(!ycbcrimg) {
        if(mode == 3)
            printf("[free_ycbcr_image] Error: address is NULL (%p)\n", ycbcrimg);
        return -1;
    }

    free(ycbcrimg);
    return 0;
}
//funcao que aloca a imagem em vetor YCbCr na memoria do sistema
int alloc_ycbcr_vector_image(YCbCr_VECTOR *ycbcrimg, int width, int height, MODE sample_mode)
{
    int size = width * height;

    if(size < 1) {
        if(mode == 3)
            printf("[alloc_ycbcr_sampled_image] Error: invalid size (%d)\n", size);
        return -1;
    }

    ycbcrimg = (YCbCr_VECTOR *) malloc(size * sizeof(YCbCr_VECTOR));

    if(!ycbcrimg) {
        if(mode == 3)
            printf("[alloc_ycbcr_sampled_image] Error: malloc error (%p)\n", ycbcrimg);
        return -1;
    }

    ycbcrimg->height = height;
    ycbcrimg->width = width;
    ycbcrimg->mode = sample_mode;
    ycbcrimg->Y = (int *) malloc(size * sizeof(int));

    if(sample_mode == S444 || sample_mode == S444_DIFF) {
        ycbcrimg->Cb = (int *) malloc(size * sizeof(int));
        ycbcrimg->Cr = (int *) malloc(size * sizeof(int));
    } else if(sample_mode == S422 || sample_mode == S422_DIFF) {
        ycbcrimg->Cb = (int *) malloc((size / 2) * sizeof(int));
        ycbcrimg->Cr = (int *) malloc((size / 2) * sizeof(int));
    }

    return 0;
}
//funcao que libera da memoria do sistema os samples criados para a imagem YCbCr
int free_ycbcr_sampled_image(YCbCr_VECTOR *ycbcrimg)
{
    if(!ycbcrimg) {
        if(mode == 3)
            printf("[free_ycbcr_sampled_image] Error: address is NULL (%p)\n", ycbcrimg);
        return -1;
    }

    free(ycbcrimg->Y);
    free(ycbcrimg->Cb);
    free(ycbcrimg->Cr);
    free(ycbcrimg);

    return 0;
}
