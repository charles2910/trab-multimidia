#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "bitmap.h"
#include "r2y.h"

extern int mode;
//funcao que converte uma imagem no formato RGB para YcbCr
void imageRgb2YCbCr (BMPIMAGE *image_RGB, YCbCr **image_YCbCr, BITMAPINFOHEADER *info_header)
{
	int tam = info_header->Height * info_header->Width;
	*image_YCbCr = (YCbCr *) malloc(tam * sizeof(YCbCr));

	if(mode == 3)
        printf("Tamanho: %d\n", tam);

	for (int i = 0; i < tam; i++)
		pixelRgb2YCbCr(&image_RGB[i], &(*image_YCbCr)[i]);

	return;
}
//funcao que converte uma imagem no formato YcbCr para RGB
void imageYCbCr2Rgb (BMPIMAGE **image_RGB, YCbCr *image_YCbCr, BITMAPINFOHEADER *info_header)
{
	int tam = info_header->Height * info_header->Width;
	*image_RGB = (BMPIMAGE *) malloc(tam * sizeof(BMPIMAGE));

	for (int i = 0; i < tam; i++)
		pixelYCbCr2Rgb(&(*image_RGB)[i], &image_YCbCr[i]);

	return;
}


/**
 * Funcao que converte apenas o o pixel 8-bit RGB para o pixel 8-bit YCbCr
 * conforme o padrao ISO/EIC 10918-5:2012 ou ITU-T T.871
 *
 */
void pixelRgb2YCbCr (BMPIMAGE *pixel_RGB, YCbCr *pixel_YCbCr)
{
	double Y;
	Y = 0.299 * pixel_RGB->R + 0.587 * pixel_RGB->G + 0.114 * pixel_RGB->B;
	pixel_YCbCr->Y = (unsigned char) min(max(0, (int) round(Y)), 255);

	double Cb;
	Cb = -0.1687 * pixel_RGB->R - 0.3313 * pixel_RGB->G + 0.5 * pixel_RGB->B + 128.0;
	pixel_YCbCr->Cb = (unsigned char) min(max(0, (int) round(Cb)), 255);

	double Cr;
	Cr = 0.5 * pixel_RGB->R - 0.4187 * pixel_RGB->G - 0.0813 * pixel_RGB->B + 128.0;
	pixel_YCbCr->Cr = (unsigned char) min(max(0, (int) round(Cr)), 255);

	return;
}

//funcao que interpreta  e convete um pixel de YCbCr para o pixel em RGB
void pixelYCbCr2Rgb (BMPIMAGE *pixel_RGB, YCbCr *pixel_YCbCr){

	double R;
	R = pixel_YCbCr->Y + 1.402 * (pixel_YCbCr->Cr - 128.0);
	pixel_RGB->R = (unsigned char) min(max(0, (int) round(R)), 255);

	double G;
	G = pixel_YCbCr->Y - 0.3441 * (pixel_YCbCr->Cb - 128.0) - 0.7141 * (pixel_YCbCr->Cr - 128.0);
	pixel_RGB->G = (unsigned char) min(max(0, (int) round(G)), 255);

	double B;
	B = pixel_YCbCr->Y + 1.772 * (pixel_YCbCr->Cb - 128.0);
	pixel_RGB->B = (unsigned char) min(max(0, (int) round(B)), 255);

	return;
}
//funcao que retorna o minimo entre dois valores
int min(int x, int y)
{
	if (x <= y) return x;
	else return y;
}
//funcao que retorna o maximo entre dois valores
int max(int x, int y)
{
	if (x >= y) return x;
	else return y;
}
