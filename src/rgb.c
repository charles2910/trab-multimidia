#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "bitmap.h"
#include "codif.h"
#include "io.h"
#include "r2y.h"
#include "sample.h"
#include "support.h"

int mode = 0;

//funcao principal que recebe o argumento e seleciona o modo necessario(compressao, ou descompressao)
int main(int argc, char *argv[])
{
   FILE *input = NULL, *output = NULL;
   BITMAPHEADER bmp_header_in, bmp_header_out;
   BMPIMAGE *Image_RGB_in, *Image_RGB_out;
   YCbCr *Image_YCbCr_in, *Image_YCbCr_out;
   YCbCr_VECTOR *Image_422_in, *Image_422_out;
   YCbCr_VECTOR *Image_422_diff_in, *Image_422_diff_out;
   FOUR22_HUFFMAN *Image_422_huffman_in, *Image_422_huffman_out;
   int tam = 0, file_size_bmp, file_size_compressed;

   if(argc == 1) { //modo de ajuda
      //print_help();
   } else if(!strcmp(argv[1], "-d")) { //modo de descompressao
      mode = 1;
   } else if(!strcmp(argv[1], "-c")) { //modo de compressao
      mode = 2;
      if(!(input = fopen(argv[2], "rb"))){ // modo de inicializacao
         printf("[rgb.c] Error: could not open input file." );
         exit(1);
      }
   } else if(!strcmp(argv[1], "--debug")) { //modo para depuracao do programa
      mode = 3;
      if(!(input = fopen(argv[2], "rb"))){
         printf("[rgb.c] Error: could not open input file." );
         exit(1);
      }
   } else {
      // erro
      //print_help()
   }

   /*Image = Bmp pixels quantity*/
   Image_422_in = (YCbCr_VECTOR *) malloc(sizeof(YCbCr_VECTOR));
   Image_422_out = (YCbCr_VECTOR *) malloc(sizeof(YCbCr_VECTOR));
   Image_422_diff_in = (YCbCr_VECTOR *) malloc(sizeof(YCbCr_VECTOR));
   Image_422_diff_out = (YCbCr_VECTOR *) malloc(sizeof(YCbCr_VECTOR));
   Image_422_huffman_in = (FOUR22_HUFFMAN *) malloc(sizeof(FOUR22_HUFFMAN));
   Image_422_huffman_out = (FOUR22_HUFFMAN *) malloc(sizeof(FOUR22_HUFFMAN));

   //inicializacao das chamadas para o modo de compressao
   if(mode == 2) {
      loadBMPHeaders (input, &bmp_header_in);
      tam = bmp_header_in.bmp_info_header.Width * bmp_header_in.bmp_info_header.Height;
      file_size_bmp = loadBMPImage(input, &bmp_header_in, &Image_RGB_in);
      imageRgb2YCbCr(Image_RGB_in, &Image_YCbCr_in, &bmp_header_in.bmp_info_header);
      four44_2_four22(Image_YCbCr_in, Image_422_in, &bmp_header_in.bmp_info_header);
      four22_2_diff(Image_422_in, Image_422_diff_in);
      diff_2_huffman(Image_422_diff_in, Image_422_huffman_in);
      file_size_compressed = write_bin(argv[3], Image_422_huffman_in, &bmp_header_in);
      printf("Tamanho imagem original: %d Bytes\n", file_size_bmp);
      printf("Tamanho imagem comprimida: %d Bytes\n", file_size_compressed);
      printf("Razão de compressão: %.2f:1\n", (float) file_size_bmp / file_size_compressed);
   } else if(mode == 1) {// inicalizacao das chamadas para o modo de descompressao
      read_bin(argv[2], Image_422_huffman_out, &bmp_header_out);
      huffman_2_diff(Image_422_diff_out, Image_422_huffman_out);
      diff_2_four22(Image_422_out, Image_422_diff_out);
      four22_2_four44(&Image_YCbCr_out, Image_422_out, &bmp_header_out.bmp_info_header);
      imageYCbCr2Rgb(&Image_RGB_out, Image_YCbCr_out, &bmp_header_out.bmp_info_header);

      if(!(output = fopen(argv[3], "wb"))){
         printf("Error: could not open output file." );
         exit(1);
      }

      storeBMPImage(output, &bmp_header_out, Image_RGB_out);
      fclose(output);
   } else if(mode == 3) {
      loadBMPHeaders (input, &bmp_header_in);
      tam = bmp_header_in.bmp_info_header.Width * bmp_header_in.bmp_info_header.Height;
      file_size_bmp = loadBMPImage(input, &bmp_header_in, &Image_RGB_in);
      imageRgb2YCbCr(Image_RGB_in, &Image_YCbCr_in, &bmp_header_in.bmp_info_header);
      four44_2_four22(Image_YCbCr_in, Image_422_in, &bmp_header_in.bmp_info_header);
      four22_2_diff(Image_422_in, Image_422_diff_in);
      diff_2_huffman(Image_422_diff_in, Image_422_huffman_in);
      file_size_compressed = write_bin(argv[3], Image_422_huffman_in, &bmp_header_in);
      printf("Tamanho imagem original: %d Bytes\n", file_size_bmp);
      printf("Tamanho imagem comprimida: %d Bytes\n", file_size_compressed);
      printf("Razão de compressão: %.2f:1\n", (float) file_size_bmp / file_size_compressed);
      free_bmp_image(Image_RGB_in);
      free_ycbcr_image(Image_YCbCr_in);
      read_bin(argv[3], Image_422_huffman_out, &bmp_header_out);
      huffman_2_diff(Image_422_diff_out, Image_422_huffman_out);
      diff_2_four22(Image_422_out, Image_422_diff_out);
      four22_2_four44(&Image_YCbCr_out, Image_422_out, &bmp_header_out.bmp_info_header);
      imageYCbCr2Rgb(&Image_RGB_out, Image_YCbCr_out, &bmp_header_out.bmp_info_header);

      if(!(output = fopen(argv[4], "wb"))){
         printf("Error: could not open output file." );
         exit(1);
      }

      storeBMPImage(output, &bmp_header_out, Image_RGB_out);

      fclose(output);
   }

   return (0);
}
