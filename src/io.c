#include <stdio.h>
#include <stdlib.h>

#include "bitmap.h"
#include "codif.h"

extern HUFFMAN_ENTRY *huffman_table;
extern int mode;

//mascara de shift
unsigned char masks[8] = {
    0b00000001,
    0b00000010,
    0b00000100,
    0b00001000,
    0b00010000,
    0b00100000,
    0b01000000,
    0b10000000
};

//funcao para escrita em binario da imagem com cabecalho no arquivo
int write_bin(char *filename, FOUR22_HUFFMAN *image_422_huffman, BITMAPHEADER *header)
{
    FILE *fout = NULL;
    HUFFMAN_VALUE *current[3], *next[3];
    int tam = image_422_huffman->height * image_422_huffman->width, file_size = 0;
    unsigned int pos_buffer = 0;
    unsigned char buffer = 0;

    fout = fopen(filename, "wb");

    if(!fout) {
        printf("[write_bin] Error: file couldn't be opened.\n");
        exit(-1);
    }

    storeBMPHeaders(fout, header);

    current[0] = &image_422_huffman->Y[0];
    next[0] = &image_422_huffman->Y[1];
    current[1] = &image_422_huffman->Cb[0];
    next[1] = &image_422_huffman->Cb[1];
    current[2] = &image_422_huffman->Cr[0];
    next[2] = &image_422_huffman->Cr[1];

    for(int pixel = 1; pixel <= tam; pixel++) {
        for(int component = 0; component < 3; component++) {
            if(pixel > tam / 2 && component > 0)
                continue; // 4:2:2 there is no Cb nor Cr after tam/2
            int prefix_size = current[component]->total_size - current[component]->manti_size;
            for(int bit_prefix = 0; bit_prefix < prefix_size; bit_prefix++) {
                if(pos_buffer > 7) {
                    fwrite(&buffer, sizeof(unsigned char), 1, fout);
                    file_size++;
                    buffer = pos_buffer = 0;
                }
                buffer = (buffer << 1) | ((current[component]->prefix & masks[prefix_size - bit_prefix - 1]) > 0);
                pos_buffer++;
            }
            for(int bit_value = 0; bit_value < current[component]->manti_size; bit_value++) {
                if(pos_buffer > 7) {
                    fwrite(&buffer, sizeof(unsigned char), 1, fout);
                    file_size++;
                    buffer = pos_buffer = 0;
                }
                buffer = (buffer << 1) | ((current[component]->mantissa & masks[current[component]->manti_size - bit_value - 1]) > 0);
                pos_buffer++;
            }
        }
        current[0] = next[0];
        if(pixel + 1 < tam)
            next[0] = &image_422_huffman->Y[pixel + 1];
        if(pixel <= tam / 2) {
            current[1] = next[1];
            current[2] = next[2];
            if(pixel + 1 < tam / 2) {
                next[1] = &image_422_huffman->Cb[pixel + 1];
                next[2] = &image_422_huffman->Cr[pixel + 1];
            }
        }
    }

    if(pos_buffer > 7) {
        fwrite(&buffer, sizeof(unsigned char), 1, fout);
        file_size++;
    } else {
        buffer = buffer<<(8 - pos_buffer);
        fwrite(&buffer, sizeof(unsigned char), 1, fout);
        file_size++;
    }

    fclose(fout);
    return file_size + header->bmp_file_header.OffBits;
}

//funcao de leitura de arquivo binario de uma imagem codificao por huffman no padrao 422
void read_bin(char *filename, FOUR22_HUFFMAN *image_422_huffman, BITMAPHEADER *header)
{
    FILE *fin = NULL;
    HUFFMAN_VALUE *current[3], *next[3];
    int tam = 0;
    unsigned int pos_buffer = 0;
    unsigned char buffer = 0, buffer_old = 0, prefix = 0, value = 0;

    if(mode == 3)
        printf("file bin: %s\n", filename);

    fin = fopen(filename, "rb");

    if(!fin) {
        printf("[read_bin] Error: file couldn't be opened.\n");
        exit(-1);
    }

    loadBMPHeaders(fin, header);

    tam = header->bmp_info_header.Width  * header->bmp_info_header.Height;

    image_422_huffman->Y = (HUFFMAN_VALUE *) malloc(tam * sizeof(HUFFMAN_VALUE));
    image_422_huffman->Cb = (HUFFMAN_VALUE *) malloc(tam / 2 * sizeof(HUFFMAN_VALUE));
    image_422_huffman->Cr = (HUFFMAN_VALUE *) malloc(tam / 2 * sizeof(HUFFMAN_VALUE));
    image_422_huffman->width =  header->bmp_info_header.Width;
    image_422_huffman->height = header->bmp_info_header.Height;

    current[0] = &image_422_huffman->Y[0];
    next[0] = &image_422_huffman->Y[1];
    current[1] = &image_422_huffman->Cb[0];
    next[1] = &image_422_huffman->Cb[1];
    current[2] = &image_422_huffman->Cr[0];
    next[2] = &image_422_huffman->Cr[1];

    buffer = (unsigned char) getc(fin);

    for(int pixel = 1; pixel <= tam; pixel++) {
        for(int component = 0; component < 3; component++) {
            if(pixel > tam / 2 && component > 0)
                continue; // 4:2:2 there is no Cb nor Cr after tam/2
            for(int bit_prefix = 0; bit_prefix < 3; bit_prefix++) {
                prefix = (prefix << 1) | ((buffer & masks[8 - pos_buffer - 1]) > 0);
                pos_buffer++;
                if(pos_buffer > 7) {
                    buffer_old = buffer;
                    buffer = (unsigned char) getc(fin);
                    pos_buffer = 0;
                }
            }
            if(prefix < 2) {
                // rewind_buffer;
                prefix = prefix >> 1;
                if(pos_buffer == 0) {
                    pos_buffer = 8;
                    ungetc(buffer, fin);
                    buffer = buffer_old;
                }
                pos_buffer--;
            }
            if(check_prefix_match(prefix)) {
                current[component]->prefix = prefix;
                int prefix_index = get_prefix_index(prefix);
                if(prefix_index < 0) {
                        printf("[read_bin] Error: incorrect prefix [%d] in pixel %d component %d\n", prefix, pixel - 1, component);
                        exit(-1);
                    }
                current[component]->total_size = get_total_size_from_index(prefix_index);
                current[component]->manti_size = get_mantissa_size_from_index(prefix_index);
                prefix = 0;
            } else {
                while(1) {
                    prefix = (prefix << 1) | ((buffer & masks[8 - pos_buffer - 1]) > 0);
                    pos_buffer++;
                    if(pos_buffer > 7) {
                        buffer_old = buffer;
                        buffer = (unsigned char) getc(fin);
                        pos_buffer = 0;
                        if((buffer_old & masks[0]) == 0)
                            break;
                        else
                            continue;
                    }
                    if((buffer & masks[8 - (pos_buffer - 1) - 1]) == 0)
                        break;
                }
                if(check_prefix_match(prefix)) {
                    current[component]->prefix = prefix;
                    int prefix_index = get_prefix_index(prefix);
                    if(prefix_index < 0) {
                        printf("[read_bin] Error: incorrect prefix [%d] in pixel %d component %d\n", prefix, pixel - 1, component);
                        exit(-1);
                    }
                    current[component]->total_size = get_total_size_from_index(prefix_index);
                    current[component]->manti_size = get_mantissa_size_from_index(prefix_index);
                    prefix = 0;
                }
            }
            for(int bit_value = 0; bit_value < current[component]->manti_size; bit_value++) {
                value = (value << 1) | ((buffer & masks[8 - pos_buffer - 1]) > 0);
                pos_buffer++;
                if(pos_buffer > 7) {
                    buffer_old = buffer;
                    buffer = (unsigned char) getc(fin);
                    pos_buffer = 0;
                }
            }
            current[component]->mantissa = value;
            value = 0;
        }

        current[0] = next[0];
        if(pixel + 1 < tam)
            next[0] = &image_422_huffman->Y[pixel + 1];
        if(pixel <= tam / 2) {
            current[1] = next[1];
            current[2] = next[2];
            if(pixel + 1 < tam / 2) {
                next[1] = &image_422_huffman->Cb[pixel + 1];
                next[2] = &image_422_huffman->Cr[pixel + 1];
            }
        }
    }

    fclose(fin);
}
