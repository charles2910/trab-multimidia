#ifndef _CODIF_H_
#define _CODIF_H_

#include "sample.h"

#define HUFFMAN_TABLE_SIZE 9

//parametros do padrao 422
typedef struct
{
    short *Y;
    short *Cb;
    short *Cr;
    unsigned int width;
    unsigned int height;
} FOUR22_DIFF;

//parametros de entrada para codificacao de huffman
typedef struct
{
    unsigned short limits[2];
    unsigned char category;
    unsigned char total_size;
    unsigned char manti_size;
    unsigned char prefix;
} HUFFMAN_ENTRY;

//valores de huffman
typedef struct
{
    unsigned char prefix;
    unsigned char mantissa;
    unsigned char total_size;
    unsigned char manti_size;
} HUFFMAN_VALUE;

//parametros utilzados no padrao 422 com codficacao de huffman
typedef struct
{
    HUFFMAN_VALUE *Y;
    HUFFMAN_VALUE *Cb;
    HUFFMAN_VALUE *Cr;
    unsigned int width;
    unsigned int height;
} FOUR22_HUFFMAN;

//declaracao das funcoes
void four22_2_diff(YCbCr_VECTOR *image_422, YCbCr_VECTOR *image_422_diff);
void diff_2_four22(YCbCr_VECTOR *image_422, YCbCr_VECTOR *image_422_diff);
void diff_2_huffman(YCbCr_VECTOR *image_422_diff, FOUR22_HUFFMAN *image_422_huffman);
void huffman_2_diff(YCbCr_VECTOR *image_422_diff, FOUR22_HUFFMAN *image_422_huffman);

const HUFFMAN_ENTRY * get_huffman_entry_from_value(int value);
const HUFFMAN_ENTRY * get_huffman_entry_from_prefix(short prefix);
unsigned char get_huffman_value(HUFFMAN_ENTRY *huffman_entry, int value);
int get_value_from_huffman(HUFFMAN_ENTRY *huffman_entry, unsigned char huffman_value);

int check_prefix_match(unsigned char prefix);
int get_prefix_index(unsigned char prefix);
unsigned char get_total_size_from_index(int index);
unsigned char get_mantissa_size_from_index(int index);

#endif