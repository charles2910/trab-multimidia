#ifndef _IO_H_
#define _IO_H_

#include "codif.h"

//cabecalho das funcoes
int write_bin(char *filename, FOUR22_HUFFMAN *image_422_huffman, BITMAPHEADER *header);
void read_bin(char *filename, FOUR22_HUFFMAN *image_422_huffman, BITMAPHEADER *header);

#endif