#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"

extern int mode;

//funcao que recebe o arquivo, le o cabecalho e captura
//os dados importante nele contigo
void loadBMPHeaders(FILE *fp, BITMAPHEADER *bmp_header)
{
     readHeader(fp, &bmp_header->bmp_file_header);
     readInfoHeader(fp, &bmp_header->bmp_info_header);
     if (bmp_header->bmp_info_header.Compression != 0) //verifica se a imagem esta comprimida
     {
          printf("This is a compressed BMP!!!");
          fclose(fp);
          return;
     }

     if(mode == 3)
          printHeaders(&bmp_header->bmp_file_header, &bmp_header->bmp_info_header);

}

//funcao que exporta os cabecalhos para serem armazenados
void storeBMPHeaders(FILE *output, BITMAPHEADER *bmp_header)
{
     storeHeader(output, &bmp_header->bmp_file_header);
     storeInfoHeader(output, &bmp_header->bmp_info_header);

     if(mode == 3)
          printHeaders(&bmp_header->bmp_file_header, &bmp_header->bmp_info_header);
}

//funcao que le os campos do cabecalho do arquivo informado
void readHeader(FILE *F, BITMAPFILEHEADER *H){
     fread(&H->Type,sizeof (unsigned short int),1,F);
     fread(&H->Size,sizeof (unsigned int),1,F);
     fread(&H->Reserved1,sizeof (unsigned short int),1,F);
     fread(&H->Reserved2,sizeof (unsigned short int),1,F);
     fread(&H->OffBits,sizeof (unsigned int),1,F);
}

//funcao que armazena os campos desejados do cabecalho do arquivo informado
void storeHeader(FILE *F, BITMAPFILEHEADER *H){
     fwrite(&H->Type,sizeof (unsigned short int),1,F);
     fwrite(&H->Size,sizeof (unsigned int),1,F);
     fwrite(&H->Reserved1,sizeof (unsigned short int),1,F);
     fwrite(&H->Reserved2,sizeof (unsigned short int),1,F);
     fwrite(&H->OffBits,sizeof (unsigned int),1,F);
}

//funcao que extrai parametros da imagem a partir do cabecalho
void readInfoHeader(FILE *F, BITMAPINFOHEADER *INFO_H){
     fread(&INFO_H->Size,sizeof (unsigned int),1,F);
     fread(&INFO_H->Width,sizeof (int),1,F);
     fread(&INFO_H->Height,sizeof (int),1,F);
     fread(&INFO_H->Planes,sizeof (unsigned short int),1,F);
     fread(&INFO_H->BitCount,sizeof (unsigned short int),1,F);
     fread(&INFO_H->Compression,sizeof (unsigned int),1,F);
     fread(&INFO_H->SizeImage,sizeof (unsigned int),1,F);
     fread(&INFO_H->XResolution,sizeof (int),1,F);
     fread(&INFO_H->YResolution,sizeof (int),1,F);
     fread(&INFO_H->NColours,sizeof (unsigned int),1,F);
     fread(&INFO_H->ImportantColours,sizeof (unsigned int),1,F);
}

//funcao que armazena os parametros extraidos da imagem a partir do cabecalho
void storeInfoHeader(FILE *F, BITMAPINFOHEADER *INFO_H){
     fwrite(&INFO_H->Size,sizeof (unsigned int),1,F);
     fwrite(&INFO_H->Width,sizeof (int),1,F);
     fwrite(&INFO_H->Height,sizeof (int),1,F);
     fwrite(&INFO_H->Planes,sizeof (unsigned short int),1,F);
     fwrite(&INFO_H->BitCount,sizeof (unsigned short int),1,F);
     fwrite(&INFO_H->Compression,sizeof (unsigned int),1,F);
     fwrite(&INFO_H->SizeImage,sizeof (unsigned int),1,F);
     fwrite(&INFO_H->XResolution,sizeof (int),1,F);
     fwrite(&INFO_H->YResolution,sizeof (int),1,F);
     fwrite(&INFO_H->NColours,sizeof (unsigned int),1,F);
     fwrite(&INFO_H->ImportantColours,sizeof (unsigned int),1,F);
}

//funcao que grava o cabecalho sequindo o modelo abaixo e contendo as informacoes
//do vetor InfoHeader
void printHeaders (BITMAPFILEHEADER *FileHeader,  BITMAPINFOHEADER *InfoHeader)
{
     /*system("cls");*/
     printf("*************** File Header ***************\n\n");

     printf("Magic number for file: %x\n", FileHeader->Type);
     printf("File's size: %d\n",FileHeader->Size);
     printf("Offset to bitmap data: %d\n", FileHeader->OffBits);

     printf("\n\n");
     printf("*************** Info Header ***************\n\n");
     printf("Info header's size: %d\n", InfoHeader->Size);
     printf("Width: %d\n", InfoHeader->Width);
     printf("Height: %d\n",InfoHeader->Height);
     printf("Color planes: %d\n", InfoHeader->Planes);
     printf("Bits per pixel: %d\n", InfoHeader->BitCount);
     printf("Compression type (0 = no compression): %d\n", InfoHeader->Compression);
     printf("Image's data size: %d\n", InfoHeader->SizeImage);
     printf("X Pixels per meter: %d\n", InfoHeader->XResolution);
     printf("Y Pixels per meter: %d\n", InfoHeader->YResolution);
     printf("Number of colors: %d\n", InfoHeader->NColours);
     printf("Numberof important colors: %d\n", InfoHeader->ImportantColours);
}

//funcao que carrega os dados da imagem para o arquivo apos o fim do cabecalho
int loadBMPImage(FILE *input, BITMAPHEADER *bmp_header, BMPIMAGE **Image) {
     int i = 0, tam = 0;

     tam = bmp_header->bmp_info_header.Height * bmp_header->bmp_info_header.Width;

     if(bmp_header->bmp_info_header.BitCount != 24) {
          printf("[loadBMPImage] Error: bmp image isn't 24 bits per pixel (it's %d bpp)\n", bmp_header->bmp_info_header.BitCount);
          exit(-1);
     }

     *Image = (BMPIMAGE *) malloc(tam * sizeof(BMPIMAGE));

     fseek(input, bmp_header->bmp_file_header.OffBits, SEEK_SET); //deslocamento de 54bytes(tamanho do header)

     for (i=0; i < tam; i++){
          (*Image)[i].B = fgetc(input);
          (*Image)[i].G = fgetc(input);
          (*Image)[i].R = fgetc(input);
     }

     return 3 * i + bmp_header->bmp_file_header.OffBits;
}

//funcao que grava no arquivo de saida a imagem e o cabecalho finalmente
int storeBMPImage(FILE *output, BITMAPHEADER *bmp_header, BMPIMAGE *Image) {
     int i = 0, tam = 0;
     unsigned int file_position = 0;

     tam = bmp_header->bmp_info_header.Height * bmp_header->bmp_info_header.Width;

     storeBMPHeaders(output, bmp_header);

     file_position = (unsigned int) ftell(output);
     if(file_position < bmp_header->bmp_file_header.OffBits) {
          do {
               fputc(' ', output);
               file_position += 1;
          } while(file_position < bmp_header->bmp_file_header.OffBits);
     }

     for (i = 0; i < tam; i++) {
          fputc(Image[i].B, output);
          fputc(Image[i].G, output);
          fputc(Image[i].R, output);
     }

     return 3 * i + bmp_header->bmp_file_header.OffBits;
}
