#ifndef _SAMPLE_H_
#define _SAMPLE_H_

#include "bitmap.h"

//declacao das funcoes
void four44_2_four22(YCbCr *image_YCbCr, YCbCr_VECTOR *image_422, BITMAPINFOHEADER *info_header);
void four22_2_four44(YCbCr **image_YCbCr, YCbCr_VECTOR *image_422, BITMAPINFOHEADER *info_header);

#endif