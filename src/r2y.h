#ifndef _R2Y_H_
#define _R2Y_H_

#include "bitmap.h"

void imageRgb2YCbCr (BMPIMAGE *image_RGB, YCbCr **image_YCbCr, BITMAPINFOHEADER *info_header);
void imageYCbCr2Rgb (BMPIMAGE **image_RGB, YCbCr *image_YCbCr, BITMAPINFOHEADER *info_header);
void pixelRgb2YCbCr (BMPIMAGE *pixel_RGB, YCbCr *pixel_YCbCr);
void pixelYCbCr2Rgb (BMPIMAGE *pixel_RGB, YCbCr *pixel_YCbCr);
int min(int x, int y);
int max(int x, int y);

#endif
